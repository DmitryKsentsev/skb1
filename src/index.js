import express from 'express';

function sum(a, b) {
   return (parseInt(a) || 0) + (parseInt(b) || 0);
}

const app = express();
app.get('/' , (req, res) => {
        const result = sum(req.query.a, req.query.b)
        res.send(result.toString());
});

app.listen('3000', function() {
   console.log("listening port 3000")  ;
})